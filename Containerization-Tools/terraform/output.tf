output "endpoints" {
  value = <<EOF

Docker IP (public):  ${aws_instance.docker.public_ip}
Docker IP (private): ${aws_instance.docker.private_ip}

For example:
   ssh -i ${var.ec2_key_name}.pem ubuntu@${aws_instance.docker.public_ip}

Podman IP (public):  ${aws_instance.podman.public_ip}
Podman IP (private): ${aws_instance.podman.private_ip}

For example:
   ssh -i ${var.ec2_key_name}.pem ubuntu@${aws_instance.podman.public_ip}

EOF

}