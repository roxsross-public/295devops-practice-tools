variable "region" {}
variable "app" {}
variable "ec2_ami" {}
variable "ec2_instance_type" {}
variable "ec2_security_group_id" {}
variable "ec2_key_name" {}