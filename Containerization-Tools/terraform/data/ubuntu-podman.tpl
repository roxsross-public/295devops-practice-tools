#!/usr/bin/env bash
sudo apt-get update
sudo apt-get install software-properties-common -y
sudo apt install -y git curl wget net-tools
sudo apt-get -y install podman 

>&2 echo Setup complete.
exit 0