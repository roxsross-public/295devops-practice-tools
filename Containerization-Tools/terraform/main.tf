provider "aws" {
  region = var.region
}

data "aws_vpc" "default" {
    default = true
}

data "aws_security_group" "selected" {
  id = var.ec2_security_group_id
}

resource "aws_instance" "docker" {
    ami                    = var.ec2_ami  
    instance_type          = var.ec2_instance_type
    vpc_security_group_ids = [ data.aws_security_group.selected.id ]
    key_name               = var.ec2_key_name
    user_data              = "${file("data/ubuntu-docker.tpl")}"
    tags = {
      "Name"    = "${var.app}-docker"
    }
}

resource "aws_instance" "podman" {
    ami                    = var.ec2_ami  
    instance_type          = var.ec2_instance_type
    vpc_security_group_ids = [ data.aws_security_group.selected.id ]
    key_name               = var.ec2_key_name
    user_data              = "${file("data/ubuntu-podman.tpl")}"
    tags = {
      "Name"    = "${var.app}-podman"
    }
}