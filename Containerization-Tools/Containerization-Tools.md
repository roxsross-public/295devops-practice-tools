[![Discord](https://img.shields.io/discord/729672926432985098?style=social&label=Discord&logo=discord)](https://discord.gg/5fqHuBq6pf)
[![YouTube Channel Subscribers](https://img.shields.io/youtube/channel/subscribers/UCxPD7bsocoAMq8Dj18kmGyQ?style=social)](https://www.youtube.com/channel/UCa-FcaB75ZtqWd1YCWW6INQ?sub_confirmation=1)

# Contenedores 🔥 🔥 
## by @roxsross

Sin lugar a dudas la tecnología de contenedores ha supuesto toda una revolución. Y dentro de los actores que se encuentran presentes en la escena, el que ha destacado desde el principio es Docker 🐳.

### Contenedores

Los contenedores permiten que una aplicación se ejecute de forma consistente y confiable, independientemente del sistema operativo o del ambiente de infraestructura. Los contenedores logran esto agrupando todo lo que un servicio necesita para ejecutarse (el código, el tiempo de ejecución, las herramientas del sistema, las bibliotecas del sistema y la configuración) y crean un paquete portátil, independiente y ejecutable.

### Ventajas de un Contenedor
- Evita fallos
- División de componentes
- Bajo coste
- Agilidad
- Escalabilidad

Los contenedores tienen la característica de servir como base para las aplicaciones nativas de la nube, contribuyendo así a alcanzar un modelo cooperativo entre entornos, tanto de cloud privadas o públicas, como híbridas.

## Herramientas que te permite la Contenerización

### Docker

La idea detrás de Docker es crear contenedores ligeros y portables para las aplicaciones software que puedan ejecutarse en cualquier máquina con Docker instalado, independientemente del sistema operativo que la máquina tenga por debajo, facilitando así también los despliegues.

[Documentación](https://docs.docker.com//)

### Podman

Podman es una herramienta nativa para Linux, de código abierto y que no utiliza ningún demonio ni procesos en segundo plano, pensada y diseñada para facilitar la búsqueda, ejecución, construcción, compartición y despliegue de aplicaciones utilizando la tecnología de contenedores e imágenes Open Containers Initiative (OCI).

[Documentación](https://podman.io/)

## Docker vs Podman: ¡Todo lo que necesitas saber!

### ¿Por qué Podman vs Docker?

Si observamos las tendencias de Google, vemos que el interés de búsqueda de Podman está en constante aumento. Es probable que veamos a Podman volverse más popular en los próximos años.

https://trends.google.com/trends/explore?q=Podman

Docker sigue siendo un jugador líder en el campo, y no creo que vaya a ninguna parte en el corto plazo, pero los gráficos nos dicen que el interés general en Docker está disminuyendo.

O los desarrolladores están perdiendo interés en Docker o han migrado a otro software que podría hacer su trabajo mejor que Docker.

https://trends.google.com/trends/explore?date=today%205-y&q=Docker


Podman está basado en Docker y fue originalmente concebido como una herramienta de debugging que terminó convirtiéndose en una alternativa o sucesor a las antiguas herramientas de gestión. 

Para facilitar la transición de uno a otro, Podman reconoce y permite utilizar los comandos de Docker. Pero esto por sí solo no convierte a Podman en un sucesor equivalente.

### Docker vs Podman | Podman vs Docker. ¿qué los diferencia?

Hay que realizar una comparación directa entre ambos sistemas si queremos poder determinar si triunfa el cuasi-estándar que ha sido popular durante años o el contendiente del duelo Docker vs. Podman 

## Ahora vamos para AWS a probar Docker y Podman

Estaremos usando `AWS CloudShell`  porque puedes ejecutar `AWS CLI` para trabajar con los servicios de AWS Y puede hacerlo sin necesidad de descargar o instalar herramientas 

En esta oportunidad vamos a estar instalando terraform para aprovisionar 2 instancias de AWS EC2 una con Docker y Otra con Podman

* Requisitos:
- Cuenta AWS
- Recuerden usar siempre la capa gratuita
- Terraform [install](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- Credenciales Programaticas AWS
- Tfenv [`install-tf-cloudshell.sh`](bonus-aws-cloudshell-tf/install-tf-cloudshell.sh)
> Tfenv la usaremos en AWS CloudShell
- Ficheros [Terraform](terraform/main.tf)
- UserData Docker [Script Docker](terraform/data/ubuntu-docker.tpl)
- UserData Podman [Script Podman](terraform/data/ubuntu-podman.tpl)

## Pasos

Si bien usas AWS CloudShell o desde tu terminal puedes correr esta excelente utilidad se llama tfenv.

- ¿Porque Tfenv?

Cuando trabajas en grandes entornos gestionados con terraform, es muy probable que tengas que trabajar a su vez con diferentes versiones de terraform, según requieran cada unos de los proyectos.

En lugar de descargar las diferentes versiones que necesitemos, renombrando los ejecutables, moviéndolos a algún directorio dentro del PATH y jugando con enlaces simbólicos, podemos símplemente usar el maravilloso tfenv.

[Documentacion TFenv](https://github.com/tfutils/tfenv)


### Ahora si vamos aprovisionar con Terraform las AWS EC2

Dentro de los Ficheros [Terraform](terraform/main.tf)

Van encontrar un tfvars que nos perfime definir la configuracion especifica del entorno

Al tener un entorno local para hacer pruebas en Terraform te puedes ayudar de un archivo llamado values.tfvars para hospedar ahí todas las variables que requieres al trabajar.

Value.tfvars [Terraform](terraform/values.tfvars)

en ese fichero tienen que reemplazar los siguientes datos de acuerdo a su configuracion que requiera tener de acuerdo a su cuenta en AWS.

```
app                    = "container"
region                 = "us-east-1"
ec2_instance_type      = "t2.micro"
ec2_key_name           = "295" #Reemplazar
ec2_security_group_id  = "sg-04627cd5684f351cc" #Reemplazar
ec2_ami                = "ami-06878d265978313ca"
```

Luego realizaro el cambio solo deben inicializar el directorio de terraform:

Inicializar: `terraform init`

Validar Terraform: `terraform validate`

Plan: `terraform plan –var-file values.tfvars -out plan.out`

Apply: `terraform apply "plan.out"`

Luego del aprovionamiento la salida es la siguiente:
```
Docker IP (public):  18.205.243.235
Docker IP (private): 172.31.82.169

For example:
   ssh -i 295.pem ubuntu@18.205.243.235

Podman IP (public):  3.88.169.4
Podman IP (private): 172.31.95.6

For example:
   ssh -i 295.pem ubuntu@3.88.169.4

```

Pueden acceder desde cualquier cliente SSH o simplemente nos vamos AWS CloudShell


### Que queda ahora probar ambas herramientas

### Docker

Docker Cheat Sheet [Doc](https://dockerlabs.collabnix.com/docker/cheatsheet/)

Verificar que Docker se este ejecutando.

`$ docker version`

- Algunos Comandos útiles para Docker

* Lista de imágenes
`$ docker image ls`
* Elimina una imagen específica
`$ docker image rm [nombre de la imagen]`
* Elimina todas las imágenes existentes
`$ docker image rm $(docker images -a -q)`
* Lista todos los contenedores existentes (en funcionamiento y no en funcionamiento)
`$ docker ps -a`
* Detener un contenedor específico
`$ docker stop [nombre del contenedor]`
* Detener todos los contenedores en funcionamiento
`$ docker stop $(docker ps -a -q)`
* Elimina un contenedor específico (solo si está detenido)
`$ docker rm [nombre del contenedor]`
* Elimina todos los contenedores (solo si están detenidos)
`$ docker rm $(docker ps -a -q)`
* Mostrar los registros de un contenedor
`$ docker logs [nombre del contenedor]`


1. Crear el archivo Dockerfile

Dockerfile [Docker](nginx-image/docker/Dockerfile)

2. Construye el contenedor

`docker build -t nginx_web:docker-1.0.0 .`

3. Corre la imagen Docker
Una vez la imagen esté creada, tu código está listo para lanzar.

`$ docker run -d -p 8080:80 nginx_web:docker-1.0.0`

### Podman

Antes que nada vamos a la documentacion para revisar su [Podman Cheat Sheet
](https://developers.redhat.com/cheat-sheets/podman-cheat-sheet)

Revisamos la Version:
```
ubuntu@ip-172-31-95-6:~$ podman version
Version:      3.4.4
API Version:  3.4.4
Go Version:   go1.17.3
Built:        Thu Jan  1 00:00:00 1970
OS/Arch:      linux/amd64
ubuntu@ip-172-31-95-6:~$ 
```

Si queremos ver los comando que puedo usar con podman

`podman -help`

Listado "Resumen"
```
Available Commands:
  attach      Attach to a running container
  build       Build an image using instructions from Containerfiles
  container   Manage containers
  cp          Copy files/folders between a container and the local filesystem
  diff        Display the changes to the object's file system
  events      Show podman events
  exec        Run a process in a running container
  generate    Generate structured data based on containers, pods or volumes
  help        Help about any command
  history     Show history of a specified image
  image       Manage images
  images      List images in local storage
  init        Initialize one or more containers
  inspect     Display the configuration of object denoted by ID
  kill        Kill one or more running containers with a specific signal
  login       Login to a container registry
  logout      Logout of a container registry
  logs        Fetch the logs of one or more containers
  network     Manage networks
  pause       Pause all the processes in one or more containers
  pod         Manage pods
  port        List port mappings or a specific mapping for the container
  ps          List containers
  pull        Pull an image from a registry
  push        Push an image to a specified destination
  restart     Restart one or more containers
  rm          Remove one or more containers
  rmi         Removes one or more images from local storage
  run         Run a command in a new container
  search      Search registry for image
  start       Start one or more containers
  stop        Stop one or more containers
  tag         Add an additional name to a local image
  version     Display the Podman version information
  volume      Manage volumes
  ```

Ejemplo:

Podemos especificar el registry si queremos buscarlo en docker-hub `docker.io`

```
podman pull docker.io/library/httpd
podman run -dt --name apache-web -p 8080:80 docker.io/library/httpd 
podman ps
podman rm apache-web
```

Y si queremos contruir nuestras imagenes usando el `build` si contamos con el Dockerfile en el directorio de trabajo pueden construir sin problemas.

Dockerfile [Podman](nginx-image/podman/Dockerfile)

`podman build -t nginx_web:podman-1.0.0 .`

Salida:
```
ubuntu@ip-172-31-95-6:~/node-app-devops$ podman images
REPOSITORY               TAG            IMAGE ID      CREATED        SIZE
localhost/nginx_web      podman-1.0.0   f43bf88b8923  2 minutes ago  101 MB
```

Ahora vamos a ejecutarlo

`podman run -d 8080:80 nginx_web:podman-1.0.0`

```
ubuntu@ip-172-31-95-6:~/$ podman ps
CONTAINER ID  IMAGE                             COMMAND               CREATED         STATUS             PORTS                   NAMES
3e419ed245a9  localhost/nginx_web:podman-1.0.0  sh -c date ; npm ...  5 seconds ago   Up 6 seconds ago   0.0.0.0:8080->80/tcp  dazzling_ganguly
```

# Notas Finales

Desde mi punto de vista, especialmente en cuanto a la seguridad; mientras Docker lleva mucho tiempo intentando ponerse al día en este aspecto, estas deficiencias se han subsanado desde el principio con Podman. 

La ausencia de un daemon centralizado y la mejor protección resultante de ello podrían convencer tarde o temprano. 

Pero sin embargo, ni siquiera Podman se libra de tener errores; todavía necesita resolver algunos problemas. Si se resuelven estos pequeños problemas con éxito, Podman ganará a Docker por un margen estrecho.

> Ahora les pregunto…. ¿Puede Podman verdaderamente sustituir a Docker?


## Contribuciones

Como siempre, cualquier contribución es bienvenida, simplemente forkeá este repo y enviame una PR/MR.  


[![Comprar](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/roxsross)